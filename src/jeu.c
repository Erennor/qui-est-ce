#include "jeu.h"
#include "ensemble.h"
#include "suspect.h"
#include <stdio.h>
#include <stdlib.h>

//fonction qui supprime tous les suspects de la liste liste ayant les attributs attribut

void supprimer_attribut(struct liste_suspects *liste, ensemble_t attribut) {
    struct suspect *cour = liste -> tete;
    struct suspect *temp;
    while (cour != liste -> queue) {
        temp = cour->suiv;
        if (ensemble_appartient(cour -> attributs, attribut)) {
            retirer_suspect(liste, cour);
        }
        cour = temp;
    }
    if (ensemble_appartient(cour -> attributs, attribut)) {
        retirer_suspect(liste, cour);
    }
}


//fonction qui supprime tous les suspects de la liste liste n'ayant pas les attributs attribut

void supprimer_non_attribut(struct liste_suspects *liste, ensemble_t attribut) {
    struct suspect *cour = liste -> tete;
    struct suspect *temp;
    while (cour != liste -> queue) {
        temp = cour->suiv;
        if (!(ensemble_appartient(cour -> attributs, attribut))) {
            retirer_suspect(liste, cour);
        }
        cour = temp;
    }
    if (!(ensemble_appartient(cour -> attributs, attribut))) {
        retirer_suspect(liste, cour);
    }
}

int main(void) {

    //Creation de la liste de suspects
    struct liste_suspects *listesuspects = creer_liste_suspects();

    //On cree les suspects avec leurs attributs
    struct suspect *andre = creer_suspect("André", HOMME + COIFFURE_CHAUVE + CHEVEUX_BLANCS + LUNETTES);
    struct suspect *philippe = creer_suspect("Philippe", HOMME + LUNETTES + CHEVEUX_ROUX + COIFFURE_COURT);
    struct suspect *jeanlouis = creer_suspect("Jean-Louis", HOMME + LUNETTES + CHEVEUX_BLANCS + COIFFURE_COURT);
    struct suspect *francois = creer_suspect("François", HOMME + CHAPEAU + CHEVEUX_BLANCS + COIFFURE_COURT);
    struct suspect *robert = creer_suspect("Robert", HOMME + BARBE + CHEVEUX_NOIRS + COIFFURE_COURT);
    struct suspect *carole = creer_suspect("Carole", FEMME + LUNETTES + CHEVEUX_ROUX + COIFFURE_COURT + CHAPEAU);
    struct suspect *melanie = creer_suspect("Mélanie", FEMME + CHEVEUX_BLONDS + COIFFURE_LONG);
    struct suspect *fabien = creer_suspect("Fabien", HOMME + MOUSTACHE + CHEVEUX_ROUX + COIFFURE_LONG);
    struct suspect *patricia = creer_suspect("Patricia", FEMME + CHEVEUX_BLANCS + COIFFURE_LONG);
    struct suspect *baptiste = creer_suspect("Baptiste", HOMME + BARBE + CHEVEUX_CHATAINS + COIFFURE_CHAUVE + MOUSTACHE);
    struct suspect *sebastien = creer_suspect("Sebastien", HOMME + CHEVEUX_ROUX + COIFFURE_COURT);
    struct suspect *olivier = creer_suspect("Olivier", HOMME + MOUSTACHE + CHEVEUX_CHATAINS + COIFFURE_COURT);
    struct suspect *nicolas = creer_suspect("Nicolas", HOMME + CHEVEUX_ROUX + COIFFURE_CHAUVE);
    struct suspect *luc = creer_suspect("Luc", HOMME + BARBE + CHEVEUX_BLONDS + COIFFURE_COURT);
    struct suspect *simon = creer_suspect("Simon", HOMME + CHAPEAU + CHEVEUX_CHATAINS + COIFFURE_COURT);
    struct suspect *maxime = creer_suspect("Maxime", HOMME + MOUSTACHE + CHEVEUX_NOIRS + COIFFURE_COURT);
    struct suspect *cedric = creer_suspect("Cédric", HOMME + LUNETTES + CHEVEUX_NOIRS + COIFFURE_CHAUVE);
    struct suspect *pierre = creer_suspect("Pierre", HOMME + CHEVEUX_CHATAINS + COIFFURE_COURT);
    struct suspect *martin = creer_suspect("Martin", HOMME + CHEVEUX_BLANCS + COIFFURE_COURT);
    struct suspect *elodie = creer_suspect("Elodie", FEMME + CHAPEAU + CHEVEUX_CHATAINS + COIFFURE_LONG);
    struct suspect *victor = creer_suspect("Victor", HOMME + BARBE + CHEVEUX_ROUX + COIFFURE_CHAUVE);
    struct suspect *georges = creer_suspect("Georges", HOMME + CHAPEAU + CHEVEUX_BLONDS + COIFFURE_COURT);
    struct suspect *thierry = creer_suspect("Thierry", HOMME + MOUSTACHE + CHEVEUX_BLONDS + COIFFURE_COURT);
    struct suspect *celine = creer_suspect("Céline", FEMME + CHEVEUX_NOIRS + COIFFURE_COURT);

    //On ajoute tous les suspects dans la liste
    ajouter_suspect(listesuspects, andre);
    ajouter_suspect(listesuspects, philippe);
    ajouter_suspect(listesuspects, jeanlouis);
    ajouter_suspect(listesuspects, francois);
    ajouter_suspect(listesuspects, robert);
    ajouter_suspect(listesuspects, carole);
    ajouter_suspect(listesuspects, melanie);
    ajouter_suspect(listesuspects, fabien);
    ajouter_suspect(listesuspects, patricia);
    ajouter_suspect(listesuspects, baptiste);
    ajouter_suspect(listesuspects, sebastien);
    ajouter_suspect(listesuspects, olivier);
    ajouter_suspect(listesuspects, nicolas);
    ajouter_suspect(listesuspects, luc);
    ajouter_suspect(listesuspects, simon);
    ajouter_suspect(listesuspects, maxime);
    ajouter_suspect(listesuspects, cedric);
    ajouter_suspect(listesuspects, pierre);
    ajouter_suspect(listesuspects, martin);
    ajouter_suspect(listesuspects, elodie);
    ajouter_suspect(listesuspects, victor);
    ajouter_suspect(listesuspects, georges);
    ajouter_suspect(listesuspects, thierry);
    ajouter_suspect(listesuspects, celine);

    
    //le vecteur question
    ensemble_t question = 0;
    
    
    char rep[30];
    while (listesuspects -> nb_suspects != 1)
    {
        if (listesuspects -> nb_suspects == 0)
        {
            printf("erreur, votre suspect n'est pas dans la liste\n");
            detruire_liste_suspects(&listesuspects);
            return 0;
        }
        
        //ensemble_afficher("question :" , question);
        
        
        //operateur aleatoire pour les questions
        srand(time(NULL));
        int ran = rand();
        ran = ran % 14;
        int random = 1;
        for (int j = 1 ; j <= ran ; j++)
        {
            random = random*2;
        }
    
        if (((random == HOMME)) && !ensemble_appartient(question, HOMME))
        {
            printf("Le suspect est-il un homme ?\n");
            question = ensemble_union(question, HOMME);
            question = ensemble_union(question, FEMME);
            fgets(rep, sizeof(rep), stdin);
            if ((*rep) == 'o')
            {
                supprimer_non_attribut(listesuspects, HOMME);
            }
            else if ((*rep) == 'n')
            {
                supprimer_attribut(listesuspects, HOMME);
                question = ensemble_union(question, MOUSTACHE);
                question = ensemble_union(question, BARBE);
            }
            else
            {
                printf("Réponse incorrect, veuillez répondre par 'o' ou 'n'");
                return 0;
            }
        }
        
        if (((random == FEMME)) && !ensemble_appartient(question, FEMME))
        {
            printf("Le suspect est-il une femme ?\n");
            question = ensemble_union(question, HOMME);
            question = ensemble_union(question, FEMME);
            fgets(rep, sizeof(rep), stdin);
            if ((*rep) == 'o')
            {
                supprimer_non_attribut(listesuspects, FEMME);
                question = ensemble_union(question, MOUSTACHE);
                question = ensemble_union(question, BARBE);
            }
            else if ((*rep) == 'n')
            {
                supprimer_attribut(listesuspects, FEMME);
            }
            else
            {
                printf("Réponse incorrect, veuillez répondre par 'o' ou 'n'");
                return 0;
            }
        }
        
        if (((random == MOUSTACHE)) && !ensemble_appartient(question, MOUSTACHE))
        {
            printf("Le suspect a-t-il une moustache ?\n");
            question = ensemble_union(question, MOUSTACHE);
            fgets(rep, sizeof(rep), stdin);
            if ((*rep) == 'o')
            {
                supprimer_non_attribut(listesuspects, MOUSTACHE);
                question = ensemble_union(question, FEMME);
                question = ensemble_union(question, HOMME);
                question = ensemble_union(question, CHAPEAU);   //car il n'y a pas de suspect avec à la fois un chapeau et une moustache
            }
            else if ((*rep) == 'n')
            {
                supprimer_attribut(listesuspects, MOUSTACHE);
            }
            else
            {
                printf("Réponse incorrect, veuillez répondre par 'o' ou 'n'");
                return 0;
            }
        }
        
        if (((random == BARBE)) && !ensemble_appartient(question, BARBE))
        {
            printf("Le suspect a-t-il une barbe ?\n");
            question = ensemble_union(question, BARBE);
            fgets(rep, sizeof(rep), stdin);
            if ((*rep) == 'o')
            {
                supprimer_non_attribut(listesuspects, BARBE);
                question = ensemble_union(question, FEMME);
                question = ensemble_union(question, HOMME);
                question = ensemble_union(question, CHAPEAU);   //car il n'y a aucun suspect avec à la fois un chapeau et de la barbe
            }
            else if ((*rep) == 'n')
            {
                supprimer_attribut(listesuspects, BARBE);
            }
            else
            {
                printf("Réponse incorrect, veuillez répondre par 'o' ou 'n'");
                return 0;
            }
        }
        
        if (((random == COIFFURE_CHAUVE)) && !ensemble_appartient(question, COIFFURE_CHAUVE))
        {
            printf("Le suspect est-il chauve ?\n");
            fgets(rep, sizeof(rep), stdin);
            if ((*rep) == 'o')
            {
                supprimer_non_attribut(listesuspects, COIFFURE_CHAUVE);
                question = ensemble_union(question, COIFFURE_CHAUVE);
                question = ensemble_union(question, COIFFURE_LONG);
                question = ensemble_union(question, COIFFURE_COURT);
            }
            else if ((*rep) == 'n')
            {
                supprimer_attribut(listesuspects, COIFFURE_CHAUVE);
                question = ensemble_union(question, COIFFURE_CHAUVE);
            }
            else
            {
                printf("Réponse incorrect, veuillez répondre par 'o' ou 'n'");
                return 0;
            }
        }
        
        if (((random == COIFFURE_LONG)) && !ensemble_appartient(question, COIFFURE_LONG))
        {
            printf("Le suspect a-t-il les cheveux longs ?\n");
            fgets(rep, sizeof(rep), stdin);
            if ((*rep) == 'o')
            {
                supprimer_non_attribut(listesuspects, COIFFURE_LONG);
                question = ensemble_union(question, COIFFURE_CHAUVE);
                question = ensemble_union(question, COIFFURE_LONG);
                question = ensemble_union(question, COIFFURE_COURT);
            }
            else if ((*rep) == 'n')
            {
                supprimer_attribut(listesuspects, COIFFURE_LONG);
                question = ensemble_union(question, COIFFURE_LONG);
            }
            else
            {
                printf("Réponse incorrect, veuillez répondre par 'o' ou 'n'");
                return 0;
            }
        }
        
        if (((random == COIFFURE_COURT)) && !ensemble_appartient(question, COIFFURE_COURT))
        {
            printf("Le suspect a-t-il les cheveux courts ?\n");
            fgets(rep, sizeof(rep), stdin);
            if ((*rep) == 'o')
            {
                supprimer_non_attribut(listesuspects, COIFFURE_COURT);
                question = ensemble_union(question, COIFFURE_CHAUVE);
                question = ensemble_union(question, COIFFURE_LONG);
                question = ensemble_union(question, COIFFURE_COURT);
            }
            else if ((*rep) == 'n')
            {
                supprimer_attribut(listesuspects, COIFFURE_COURT);
                question = ensemble_union(question, COIFFURE_COURT);
            }
            else
            {
                printf("Réponse incorrect, veuillez répondre par 'o' ou 'n'");
                return 0;
            }
        }
        
        if (((random == CHEVEUX_NOIRS)) && !ensemble_appartient(question, CHEVEUX_NOIRS))
        {
            printf("Le suspect a-t-il les cheveux noirs ?\n");
            fgets(rep, sizeof(rep), stdin);
            if ((*rep) == 'o')
            {
                supprimer_non_attribut(listesuspects, CHEVEUX_NOIRS);
                question = ensemble_union(question, CHEVEUX_NOIRS);
                question = ensemble_union(question, CHEVEUX_CHATAINS);
                question = ensemble_union(question, CHEVEUX_BLANCS);
                question = ensemble_union(question, CHEVEUX_ROUX);
                question = ensemble_union(question, CHEVEUX_BLONDS);
            }
            else if ((*rep) == 'n')
            {
                supprimer_attribut(listesuspects, CHEVEUX_NOIRS);
                question = ensemble_union(question, CHEVEUX_NOIRS);
            }
            else
            {
                printf("Réponse incorrect, veuillez répondre par 'o' ou 'n'");
                return 0;
            }
        }
        
        if (((random == CHEVEUX_CHATAINS)) && !ensemble_appartient(question, CHEVEUX_CHATAINS))
        {
            printf("Le suspect est-il chatain ?\n");
            fgets(rep, sizeof(rep), stdin);
            if ((*rep) == 'o')
            {
                supprimer_non_attribut(listesuspects, CHEVEUX_CHATAINS);
                question = ensemble_union(question, CHEVEUX_NOIRS);
                question = ensemble_union(question, CHEVEUX_CHATAINS);
                question = ensemble_union(question, CHEVEUX_BLANCS);
                question = ensemble_union(question, CHEVEUX_ROUX);
                question = ensemble_union(question, CHEVEUX_BLONDS);
            }
            else if ((*rep) == 'n')
            {
                supprimer_attribut(listesuspects, CHEVEUX_CHATAINS);
                question = ensemble_union(question, CHEVEUX_CHATAINS);
            }
            else
            {
                printf("Réponse incorrect, veuillez répondre par 'o' ou 'n'");
                return 0;
            }
        }
        
        if (((random == CHEVEUX_BLANCS)) && !ensemble_appartient(question, CHEVEUX_BLANCS))
        {
            printf("Le suspect a-t-il les cheveux blancs ?\n");
            fgets(rep, sizeof(rep), stdin);
            if ((*rep) == 'o')
            {
                supprimer_non_attribut(listesuspects, CHEVEUX_BLANCS);
                question = ensemble_union(question, CHEVEUX_NOIRS);
                question = ensemble_union(question, CHEVEUX_CHATAINS);
                question = ensemble_union(question, CHEVEUX_BLANCS);
                question = ensemble_union(question, CHEVEUX_ROUX);
                question = ensemble_union(question, CHEVEUX_BLONDS);
            }
            else if ((*rep) == 'n')
            {
                supprimer_attribut(listesuspects, CHEVEUX_BLANCS);
                question = ensemble_union(question, CHEVEUX_BLANCS);
            }
            else
            {
                printf("Réponse incorrect, veuillez répondre par 'o' ou 'n'");
                return 0;
            }
        }
        
        if (((random == CHEVEUX_ROUX)) && !ensemble_appartient(question, CHEVEUX_ROUX))
        {
            printf("Le suspect est-il roux ?\n");
            fgets(rep, sizeof(rep), stdin);
            if ((*rep) == 'o')
            {
                supprimer_non_attribut(listesuspects, CHEVEUX_ROUX);
                question = ensemble_union(question, CHEVEUX_NOIRS);
                question = ensemble_union(question, CHEVEUX_CHATAINS);
                question = ensemble_union(question, CHEVEUX_BLANCS);
                question = ensemble_union(question, CHEVEUX_ROUX);
                question = ensemble_union(question, CHEVEUX_BLONDS);
            }
            else if ((*rep) == 'n')
            {
                supprimer_attribut(listesuspects, CHEVEUX_ROUX);
                question = ensemble_union(question, CHEVEUX_ROUX);
            }
            else
            {
                printf("Réponse incorrect, veuillez répondre par 'o' ou 'n'");
                return 0;
            }
        }
        
        if (((random == CHEVEUX_BLONDS)) && !ensemble_appartient(question, CHEVEUX_BLONDS))
        {
            printf("Le suspect est-il blond ?\n");
            fgets(rep, sizeof(rep), stdin);
            if ((*rep) == 'o')
            {
                supprimer_non_attribut(listesuspects, CHEVEUX_BLONDS);
                question = ensemble_union(question, CHEVEUX_NOIRS);
                question = ensemble_union(question, CHEVEUX_CHATAINS);
                question = ensemble_union(question, CHEVEUX_BLANCS);
                question = ensemble_union(question, CHEVEUX_ROUX);
                question = ensemble_union(question, CHEVEUX_BLONDS);
            }
            else if ((*rep) == 'n')
            {
                supprimer_attribut(listesuspects, CHEVEUX_BLONDS);
                question = ensemble_union(question, CHEVEUX_BLONDS);
            }
            else
            {
                printf("Réponse incorrect, veuillez répondre par 'o' ou 'n'");
                return 0;
            }
        }
        
        if (((random == LUNETTES)) && !ensemble_appartient(question, LUNETTES))
        {
            printf("Le suspect porte-t-il des lunettes ?\n");
            question = ensemble_union(question, LUNETTES);
            fgets(rep, sizeof(rep), stdin);
            if ((*rep) == 'o')
            {
                supprimer_non_attribut(listesuspects, LUNETTES);
            }
            else if ((*rep) == 'n')
            {
                supprimer_attribut(listesuspects, LUNETTES);
            }
            else
            {
                printf("Réponse incorrect, veuillez répondre par 'o' ou 'n'");
                return 0;
            }
        }
        
        if (((random == CHAPEAU)) && !ensemble_appartient(question, CHAPEAU))
        {
            printf("Le suspect porte-t-il un chapeau ?\n");
            question = ensemble_union(question, CHAPEAU);
            fgets(rep, sizeof(rep), stdin);
            if ((*rep) == 'o')
            {
                supprimer_non_attribut(listesuspects, CHAPEAU);
                question = ensemble_union(question, BARBE);     //car aucun susect n'a à la fois un chapeau et
                question = ensemble_union(question, MOUSTACHE); //de la barbe ou de la moustache
            }
            else if ((*rep) == 'n')
            {
                supprimer_attribut(listesuspects, CHAPEAU);
            }
            else
            {
                printf("Réponse incorrect, veuillez répondre par 'o' ou 'n'");
                return 0;
            }
        }
    }
    
    printf("Le suspect est : %s \n", listesuspects -> queue -> nom);
    
    //affiche_liste_suspects(listesuspects);
    detruire_liste_suspects((&listesuspects));
    //affiche_liste_suspects(listesuspects);

    return 0;
}
