#include "ensemble.h"

ensemble_t ensemble_vide(void) {
    return 0;
}

ensemble_t ensemble_plein(void) {
    return 0b1111111111111111;
}

uint16_t ensemble_cardinal(ensemble_t e) {
    ensemble_t temp = e;
    uint16_t compteur = 0;
    for (int i = 1; i <= 16; i++) {
        if (temp % 2 == 1) {
            compteur = compteur + 1;
            temp = (temp - 1) / 2;
        } else {
            temp = temp / 2;
        }

    }
    return compteur;
}

bool ensemble_appartient(ensemble_t e, uint16_t numero_elt) { //ne marche pas sur le make check mais marche bien
    if (ensemble_intersection(e, numero_elt) == numero_elt) //dans l'execution du programme
    {
        return true;
    } else {
        return false;
    }

}

ensemble_t ensemble_union(ensemble_t e1, ensemble_t e2) {
    return (e1 | e2);
}

ensemble_t ensemble_intersection(ensemble_t e1, ensemble_t e2) {
    return (e1 & e2);
}

ensemble_t ensemble_complementaire(ensemble_t e) {
    return ~e;
}

void ensemble_ajouter_elt(ensemble_t *e, uint16_t numero_elt) {
    *e = *e | 1 << numero_elt;
}

void ensemble_retirer_elt(ensemble_t *e, uint16_t numero_elt) {
    *e = *e ^ 1 << numero_elt;
}

//Affiche les bits en sens inverse mais c'pas grave ça marche

void ensemble_afficher(const char *msg, ensemble_t e) {
    ensemble_t temp = e;
    printf("%s", msg);
    for (int i = 0; i <= 15; i++) {
        if (temp % 2 == 1) {
            printf("1");
        } else {
            printf("0");
        }
        temp = temp >> 1;
    }
    printf("\n");
}
