#include "suspect.h"

struct suspect *creer_suspect(const char *name, ensemble_t attributs) {
    if (name == NULL) {
        return NULL;
    }
    char *chaine = malloc(sizeof (char) *(strlen(name) + 1));
    strcpy(chaine, name);
    struct suspect *nouveau = malloc(sizeof (struct suspect));
    nouveau -> attributs = attributs;
    nouveau -> nom = chaine;
    nouveau -> suiv = NULL;
    nouveau -> prec = NULL;
    return nouveau;

}

struct liste_suspects *creer_liste_suspects(void) {
    struct liste_suspects *nouvelleliste = malloc(sizeof (struct liste_suspects));
    nouvelleliste -> nb_suspects = 0;
    nouvelleliste -> queue = NULL;
    nouvelleliste -> tete = NULL;
    return nouvelleliste;
}

void detruire_liste_suspects(struct liste_suspects **l) {
    if ((*l) ->tete == NULL) {
        //printf("La liste était déjà vide \n");
        free(*l);
    } else {
        struct suspect *cour = (*l) -> tete;
        struct suspect *temp;
        while (cour != (*l) -> queue) {
            temp = cour->suiv;
            retirer_suspect((*l), cour);
            cour = temp;
        }
        free((*l)->tete->suiv);
        free((*l)->tete->prec);
        free((*l)->tete->nom);
        free((*l)->tete);
        free(*l);
    }
}

void ajouter_suspect(struct liste_suspects *liste, struct suspect *suspect) {
    if (liste -> tete == NULL) {
        liste -> nb_suspects = liste -> nb_suspects + 1;
        liste -> tete = suspect;
        liste -> queue = suspect;
        //printf("la liste était initialement vide\n");
    } else {
        liste -> nb_suspects = liste -> nb_suspects + 1;
        suspect -> suiv = liste -> tete;
        liste -> tete -> prec = suspect;
        suspect -> prec = liste -> queue;
        liste -> queue -> suiv = suspect;
        liste -> queue = suspect;
        //printf("la liste n'était pas initialment vide\n");
    }
}

void retirer_suspect(struct liste_suspects *liste, struct suspect *suspect) {
    if (liste -> tete == NULL) {
        printf("La liste de suspects est vide \n");
    } else if (liste -> tete == liste -> queue) {
        liste -> tete = NULL;
        liste -> queue = NULL;
        liste -> nb_suspects = 0;
        free(suspect->nom);
        free(suspect);
    } else {
        if (suspect == liste -> tete) //suspect = liste->tete
        {
            liste -> tete = liste -> tete -> suiv;
            liste -> tete -> prec = NULL;
        } else if (suspect == liste -> queue) //suspect = liste->queue
        {
            liste -> queue = liste -> queue -> prec;
            liste -> queue -> suiv = NULL;
        } else {
            suspect -> suiv -> prec = suspect -> prec;
            suspect -> prec -> suiv = suspect -> suiv;
        }
        liste -> nb_suspects = liste -> nb_suspects - 1;
        free(suspect->nom);
        free(suspect);
    }
}

void affiche_liste_suspects(struct liste_suspects *l) {
    struct suspect *cour = l -> tete;
    printf("La liste contient %d suspects \n", l -> nb_suspects);
    for (int i = 0; i < l -> nb_suspects; i++) {
        printf("%s\n", cour -> nom);
        cour = cour -> suiv;
    }
}
